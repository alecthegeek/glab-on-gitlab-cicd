# Glab On GitLab CICD


A demo project to demostrate some of the uses of the GitLab CI tool Glab, particualry when used with GitLab CI/CICD


It's a very simple project with only a single file. This README.

However the CI/CD is more complex.

We also make use of the Git config setting `core.hooksPath` setting to extend the use of Glab into Git hook scripts

To clone this repo 

```sh
git clone --config core.hooksPath="./.hooks"  <clone-url>
```

or in an exisiting clone run

```sh
git config core.hooksPath="./hooks"
```

# What this repo does

1. It uses GitFlow

Every time CI/CD is run on a branch a index.html page is create with new content. It can then be deployed on GitHub Pages

<!-- Content added here -->
